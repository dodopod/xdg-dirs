from os import environ
from os.path import isabs
from pathlib import Path


def _find_dir(env_var, default=None):
    """Returns a path from an environment variable.

    Args:
        env_var (str): Environment variable containing the path.

        default (Path, optional): Default path, if the environment variable
        doesn't exist or is empty.

    Returns:
        Path or NoneType: Either the path from the environment variable, the
        default, or None.

        As per the `specification`_, the path *must* be absolute. If not, it
        will be considered invalid, and this function will return None.

        .. _specification: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
    """

    path = environ.get(env_var)
    if path and isabs(path):
        return Path(path)
    elif default and isabs(default):
        return default
    else:
        return None


def _find_dirs(env_var, default=[]):
    """Returns a list of paths from an environment variable.

    Args:
        env_var (str): Environment variable containing a colon-separated list of
        paths.

        default (list of Path): Default paths, if the environment variable doesn't
        exist or is empty.

    Returns:
        list of Path: A list containing the paths in the environment variable,
        or the default paths.

        As per the `specification`_, all paths must be absolute. Any relative
        paths will be omitted from the list.

        .. _specification: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
    """

    return [Path(p) for p in environ.get(env_var, '').split(':') if isabs(p)] \
        or [p for p in default if isabs(p)] \
        or []


xdg_cache_home = _find_dir('XDG_CACHE_HOME', Path.home()/'.cache')
xdg_config_home = _find_dir('XDG_CONFIG_HOME', Path.home()/'.config')
xdg_data_home = _find_dir('XDG_DATA_HOME', Path.home()/'.local/share')
xdg_state_home = _find_dir('XDG_STATE_HOME', Path.home()/'.local/state')

xdg_runtime_dir = _find_dir('XDG_RUNTIME_DIR')

xdg_data_dirs = _find_dirs('XDG_DATA_DIRS',
                           [Path('/usr/local/share'), Path('/usr/share')])
xdg_config_dirs = _find_dirs('XDG_CONFIG_DIRS', ['/etc/xdg'])
