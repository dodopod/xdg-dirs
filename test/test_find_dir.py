from os import environ
from pathlib import Path
from unittest import TestCase

from xdg_dirs import _find_dir


class FindDirTestCase(TestCase):
    def setUp(self):
        if 'FOO' in environ:
            del environ['FOO']

    def test_absolute(self):
        environ['FOO'] = '/'
        self.assertEqual(_find_dir('FOO'), Path('/'))


    def test_relative(self):
        environ['FOO'] = '.'
        self.assertIsNone(_find_dir('FOO'))


    def test_no_var_default(self):
        self.assertEqual(_find_dir('FOO', Path('/')), Path('/'))


    def test_no_var_no_default(self):
        self.assertIsNone(_find_dir('FOO'))


    def test_empty_default(self):
        environ['FOO'] = ''
        self.assertEqual(_find_dir('FOO', Path('/')), Path('/'))


    def test_empty_no_default(self):
        environ['FOO'] = ''
        self.assertIsNone(_find_dir('FOO'))
    

    def test_no_var_relative_default(self):
        self.assertIsNone(_find_dir('FOO', Path('.')))
