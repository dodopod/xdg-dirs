from os import environ
from pathlib import Path
from unittest import TestCase

from xdg_dirs import _find_dirs


class FindDirTestCase(TestCase):
    def setUp(self):
        if 'FOO' in environ:
            del environ['FOO']


    def test_no_var_default(self):
        self.assertEqual(_find_dirs('FOO', [Path('/')]), [Path('/')])


    def test_no_var_no_default(self):
        self.assertEqual(_find_dirs('FOO'), [])


    def test_no_var_relative_default(self):
        self.assertEqual(_find_dirs('FOO', [Path('/'), Path('.')]),
            [Path('/')])


    def test_empty_default(self):
        environ['FOO'] = ''
        self.assertEqual(_find_dirs('FOO', [Path('/')]), [Path('/')])


    def test_empty_no_default(self):
        environ['FOO'] = ''
        self.assertEqual(_find_dirs('FOO'), [])


    def test_single_absolute(self):
        environ['FOO'] = '/'
        self.assertEqual(_find_dirs('FOO'), [Path('/')])


    def test_single_relative(self):
        environ['FOO'] = '.'
        self.assertEqual(_find_dirs('FOO'), [])


    def test_multi_empty_first(self):
        environ['FOO'] = ':/a:/b'
        self.assertEqual(_find_dirs('FOO'), [Path('/a'), Path('/b')])


    def test_multi_empty_middle(self):
        environ['FOO'] = '/a::/b'
        self.assertEqual(_find_dirs('FOO'), [Path('/a'), Path('/b')])


    def test_multi_empty_last(self):
        environ['FOO'] = '/a:/b:'
        self.assertEqual(_find_dirs('FOO'), [Path('/a'), Path('/b')])


    def test_multi_relative(self):
        environ['FOO'] = '/:.'
        self.assertEqual(_find_dirs('FOO'), [Path('/')])
